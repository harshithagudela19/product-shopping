from django.urls import path

from .views import ProductList, ProductDetails, OrderProductDetails, OrderProductList, OrderList, OrderDetails

urlpatterns = [
    path('products/', ProductList.as_view({'get': 'list'}), name='productlist'),
    path('product/<pk>',
         ProductDetails.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='productdetails'),
    path('orders/', OrderList.as_view({'get': 'list'}), name='orderlist'),
    path('orders/<str:pk>',
         OrderDetails.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='orderdetails'),
    path('orderproducts/', OrderProductList.as_view({'get': 'list'}), name='orderproductlist'),
    path('orderproducts/<str:pk>',
         OrderProductDetails.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='orderproductdetails'),
]