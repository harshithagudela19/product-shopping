from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Product(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    image_link = models.URLField()
    price = models.FloatField()
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.title


STATUS_CHOICES = (
    ('new', 'new'),
    ('paid', 'paid')
)

MODE_OF_PAYMENT = (
    ('cash', 'cash'),
)


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    total = models.IntegerField()
    created_order_at = models.DateField(auto_now_add=True)
    updated_order_at = models.DateField(auto_now=True)
    status = models.CharField(choices=STATUS_CHOICES, max_length=5)
    mode_of_payment = models.CharField(choices=MODE_OF_PAYMENT, max_length=10)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return self.product.title


class OrderProduct(models.Model):
    orderId = models.ForeignKey(Order, on_delete=models.CASCADE)
    productId = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.FloatField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.productId.title