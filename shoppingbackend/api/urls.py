from django.urls import path
from .views import RegisterViewSet, ChangePasswordViewSet, UpdateProfileViewSet, LogoutViewSet, LogoutAllViewSet
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

urlpatterns = [
    path('login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', RegisterViewSet.as_view(), name='auth_register'),
    path('change_password/<int:pk>/', ChangePasswordViewSet.as_view(), name='auth_change_password'),
    path('update_profile/<int:pk>/', UpdateProfileViewSet.as_view(), name='auth_update_profile'),
    path('logout/', LogoutViewSet.as_view(), name='auth_logout'),
    path('logout_all/', LogoutAllViewSet.as_view(), name='auth_logout_all'),
]